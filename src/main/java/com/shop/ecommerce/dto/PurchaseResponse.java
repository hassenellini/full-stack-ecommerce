package com.shop.ecommerce.dto;

import lombok.Data;

@Data
public class PurchaseResponse {

    // @Data generate constructor only if field is final
    private final String orderTrackingNumber;

}
