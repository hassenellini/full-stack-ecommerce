package com.shop.ecommerce.dto;

import com.shop.ecommerce.dao.entity.Address;
import com.shop.ecommerce.dao.entity.Customer;
import com.shop.ecommerce.dao.entity.Order;
import com.shop.ecommerce.dao.entity.OrderItem;
import lombok.Data;

import java.util.Set;

@Data
public class Purchase {

    private Customer customer;

    private Address shippingAddress;

    private Address billingAddress;

    private Order order;

    private Set<OrderItem> orderItems;
}
