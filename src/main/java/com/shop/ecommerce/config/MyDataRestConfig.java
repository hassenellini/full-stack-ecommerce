package com.shop.ecommerce.config;

import com.shop.ecommerce.dao.entity.Country;
import com.shop.ecommerce.dao.entity.Product;
import com.shop.ecommerce.dao.entity.ProductCategory;
import com.shop.ecommerce.dao.entity.State;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Configuration
@AllArgsConstructor
public class MyDataRestConfig implements RepositoryRestConfigurer {

    private EntityManager entityManager;

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {

        HttpMethod[] unsupporetdActions = {HttpMethod.PUT, HttpMethod.POST, HttpMethod.DELETE};

        disableHttpMethodes(Product.class, config, unsupporetdActions);
        disableHttpMethodes(ProductCategory.class, config, unsupporetdActions);
        disableHttpMethodes(Country.class, config, unsupporetdActions);
        disableHttpMethodes(State.class, config, unsupporetdActions);

        exposeIds(config);
    }

    private void disableHttpMethodes(Class clazz, RepositoryRestConfiguration config, HttpMethod[] unsupporetdActions) {
        config.getExposureConfiguration()
                .forDomainType(clazz)
                .withItemExposure(((metdata, httpMethods) -> httpMethods.disable(unsupporetdActions)))
                .withCollectionExposure(((metdata, httpMethods) -> httpMethods.disable(unsupporetdActions)));
    }

    // expose ids for entities in Spring Data Rest
    private void exposeIds(RepositoryRestConfiguration config) {
        // get a list of all enities classes from the entity manager
        Set<EntityType<?>> entityTypes = entityManager.getMetamodel().getEntities();

        // create an array of the entity types
        List<Class> entityClasses = new ArrayList<>();
        // get the entity types for the entities
        for (EntityType entityType : entityTypes) {
            entityClasses.add(entityType.getJavaType());
        }
        // expose the entity ids for the array of entity/domain types
        Class[] domainTypes = entityClasses.toArray(new Class[0]);
        config.exposeIdsFor(domainTypes);

    }
}
